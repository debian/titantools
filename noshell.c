/* This tool suite was written by and is copyrighted by Brad Powell, Matt */
/* Archibald, and Dan Farmer 1999, 2000, 2001                             */
/* The copyright holder disclaims all responsibility or liability with    */ 
/* respect to its usage or its effect upon hardware or computer           */ 
/* systems, and maintains copyright as set out in the "LICENSE"           */  
/* document which accompanies distribution.                               */ 
/* additional fixes from Andrew Wansink <andy@sharinga.com>               */
/* Titan version 4.0.11 April 22 11:21:02 PDT 2001                        */


#include <syslog.h>
#include <unistd.h>
#include <signal.h>
 
/* this is so that the what(1) command will show the version */
#ifndef lint 
static char sccsid[] = "@(#)noshell.c  bpowell-  Titan 4.0.11 4/22/01";
#endif /* not lint */ 

#ifndef NSIG

/* SunOS8 */
#ifdef _sys_siglistn                                          
#define NSIG _sys_siglistn                                    
#endif /* _sys_siglistn */                                    
                                                              
/* Linux */                                                   
#ifdef _NSIG                                                  
#define NSIG _NSIG                                            
#endif /* _NSIG */                                            
                                                              
#endif /* NSIG */   

#define perrorexit(s) { perror(s); exit(1); }

int main (int argc, char **argv)
{
	extern char **environ;
	int i;

	/* Useless assignments to clear compiler warning */   
	i = argc;                                             
	i = (int) sccsid[0];      


	/*
	 * Fix the environment.
	 */

	if (environ != 0)
		environ[0] = 0;

	/*
	 * Fix the signals.
 	 */

	for (i = 1; i < NSIG; i++)
		(void) signal (i, SIG_IGN);

	/*
	 * Log the login attempt.
 	 */

	openlog (argv[0], LOG_PID, LOG_AUTH);
	syslog
	(
		LOG_WARNING,
		"Noshell warning: user %s login from a disabled shell",
		getlogin()
	);
	closelog();

	return 0;
}
